import { Component } from "@angular/core";

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
  })
  export class HomeComponent {
    title = "Orders Management System";
    author = "Abdul Hanan Zaroo"
    constructor(){
        console.log("HomeComponent initiated ...");
    }
  }