import { Component } from "@angular/core";
import {OrdersService} from "../../services/orders.service";
import {Router} from '@angular/router';

@Component({
    selector: 'home',
    templateUrl: './orderList.component.html',
    // styleUrls: ['./home.component.css'],
    providers : [OrdersService]

})
export class OrderListComponent {

    orders;
    frequency;

    constructor(private ordersService:OrdersService, private router:Router){
        console.log("OrderListComponent initiated ...");
    }

    ngOnInit(){
        this.getFrequency();
        this.getOrders();
    }

    getOrders(){
        this.ordersService.getOrders().subscribe(
            res => {
                this.orders = res.data.orders;
            }
        )
    }

    viewOrder(id){
        this.router.navigate(['orders/view/', id]) 
    }

    updateFrequency(frequency){
        this.ordersService.updateFrequency(frequency).subscribe(
            res => {
                this.frequency = res.data.frequency;
                alert("Frequency has been updated.")
            }
        );
    }

    getFrequency(){
        this.ordersService.getFrequency().subscribe(
            res => {
                this.frequency = res.data.frequency;
            }
        );
    }



    
}