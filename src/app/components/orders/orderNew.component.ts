import { Component } from "@angular/core";
import {OrdersService} from "../../services/orders.service";
import {PaymentsService} from "../../services/payments.service";
import {Router} from '@angular/router';

@Component({
    selector: 'home',
    templateUrl: './orderNew.compnent.html',
    // styleUrls: ['./home.component.css'],
    providers : [OrdersService, PaymentsService] 
  })
  export class OrderNewComponent {

    order = {
      order_no : null,
      item_name : null,
      amount : null,
    }

    constructor(private ordersService:OrdersService, private paymentsService:PaymentsService, private router:Router){
        console.log("OrderNewComponent initiated ...");
    }

    back(){
      this.router.navigate(['orders'])           
    }

    submitForm(){
      
      // validate inputs (need a better solution probably using a validation npm package)
      if (!this.order.order_no || !this.order.item_name || !this.order.amount ) {
        alert("Please fill all fields");
        return
      }

      this.ordersService.createOrder(this.order)
      .subscribe(res => {
        
        var order = res.data.order;

        // Make call to payment app to create a payment with mock auth
        this.paymentsService.createPayment(order).subscribe(res => {
          
          var order_id = res.data.payment.order_id
          var status = res.data.payment.status
          if (status == "declined") alert("Payment for the order has been declined. Order will be cancelled.")
          if (status == "confirmed") alert("Payment for the order has been Confirmed. Order will be delived soon.")
          // Make call to the update order enpoint depending on the status
          this.ordersService.updateOrderStatus(order_id,status).subscribe(
            res => {
              alert("Order created !")
              this.router.navigate(['orders/view', order_id])
            }
          )

        })

      })
    }
  }