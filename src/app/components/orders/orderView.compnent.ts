import { OnInit, Component} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {OrdersService} from "../../services/orders.service";
import {Router} from '@angular/router';

@Component({
    selector: 'home',
    templateUrl: './orderView.component.html',
    // styleUrls: ['./home.component.css'],
    providers : [OrdersService]
})
export class OrderViewComponent {
  order = {
    id : null,
    order_no : null,
    item_name : null,
    amount : null,
    status : null
  }
  order_id; 

  constructor(private route: ActivatedRoute, private ordersService:OrdersService, private router:Router){
    console.log("orderViewComponent initiated ...");
  }

  ngOnInit(){

    // Check the id in url
    this.route.params.subscribe(params => {
      
      // Send get request to backend to get order details
      this.ordersService.getOrderDetails(params['order_id']).subscribe(res => {
        this.order.id = res.data.order.id;
        this.order.order_no = res.data.order.order_no;
        this.order.item_name = res.data.order.item_name;
        this.order.amount = res.data.order.amount;
        this.order.status = res.data.order.status;
      })

    })
  }

  cancelOrder(id){
    alert("Are you sure you want to cancel this order?")
    this.ordersService.updateOrderStatus(id,"cancelled").subscribe(res => {
      this.order.status = res.data.order.status;
      this.router.navigate(['orders/view',id])
    })
  }

  back(){
    this.router.navigate(['orders']);
  }
  
}
