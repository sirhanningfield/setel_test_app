import { Component } from '@angular/core';
import {OrdersService} from "../../../services/orders.service";
import {PaymentsService} from "../../../services/payments.service";

@Component({
  selector: 'nav-bar',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css'],
  providers : [OrdersService, PaymentsService]
})
export class NavComponent {
    constructor(private ordersService:OrdersService, private paymentsService:PaymentsService){
        console.log("NavComponent initiated ...");
    }

    checkHealth(service){
      console.log(service);
      switch (service) {
        case "payment":
          this.paymentsService.checkHealth().subscribe(
            res => {
              alert("Payment api status: "+res.data.status)
            }
          )
          break;
        case "order":
          this.ordersService.checkHealth().subscribe(
            res => {
              alert("Order api status: "+res.data.status)
            }
          )
          break;
      }
    }
}
