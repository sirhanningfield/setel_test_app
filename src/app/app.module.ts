import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavComponent } from '../app/components/partials/nav/nav.component';
import { HomeComponent } from '../app/components/home/home.component';
import { OrderListComponent } from './components/orders/orderList.component';
import { OrderNewComponent } from './components/orders/orderNew.component';
import { OrderViewComponent } from './components/orders/orderView.compnent';
import { PaymentListComponent } from './components/payments/paymentList.component';

@NgModule({
  declarations: [
    AppComponent, 
    NavComponent, 
    HomeComponent, 
    OrderListComponent,
    OrderViewComponent,
    OrderNewComponent,
    PaymentListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path : "",
        component : HomeComponent
      },
      {
        path: "orders/view/:order_id",
        component : OrderViewComponent
      },
      {
        path: "orders/new",
        component : OrderNewComponent
      },
      {
        path : "orders",
        component : OrderListComponent
      },
      {
        path : "orders",
        component : OrderListComponent
      },
      {
        path : "payments",
        component : PaymentListComponent
      }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
