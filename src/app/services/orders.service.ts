import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import "rxjs/add/Observable/throw";
import { Observable } from 'rxjs/Observable';
import {Router} from '@angular/router';

import {API_BASE_URL} from "../../../src/constants"
import { errorHandler } from "@angular/platform-browser/src/browser";


@Injectable()

export class OrdersService{

    constructor( private http: Http){
        console.log("OrdersService Initialized ...");
    }

    getOrders(){
        var url = API_BASE_URL+"/orders"
        return this.http.get(url)
            .map(res => res.json())
            .catch(this.handleError);
    }

    getOrderDetails(id){
        var url = API_BASE_URL+"/orders/"+id;
        return this.http.get(url)
            .map(res => res.json())
            .catch(this.handleError);
    }

    handleError(error){
        
        var response = JSON.parse(error._body)
        if(response.status == 0) alert(response.error_message)
        return Observable.throw(response)

    }

    updateFrequency(frequency){
        let params = {
            frequency : frequency
        }
        var url = API_BASE_URL+"/orders/frequency";
        return this.http.put(url,params)
            .map(res => res.json());
    }

    getFrequency(){
        return this.http.get(API_BASE_URL+"/orders/frequency")
            .map(res =>res.json())
    }

    updateOrderStatus(id, status){
        var url = API_BASE_URL+"/orders/status";
        var params = {
            order_id : id,
            status : status
        }
        return this.http.post(url,params)
            .map(res =>res.json())
    }

    createOrder(order){
        var params = order;
        var url = API_BASE_URL+"/orders"
        return this.http.post(url,params)
        .map(res => res.json()).catch(this.handleError)
    }

    checkHealth(){
        var url = API_BASE_URL+"/orders/health";
        return this.http.get(url)
        .map(res => res.json()).catch(this.handleError)
    }
}