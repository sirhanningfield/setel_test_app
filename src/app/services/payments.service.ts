import {Injectable} from "@angular/core";
import {Http, Headers} from "@angular/http";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import "rxjs/add/Observable/throw";
import { Observable } from 'rxjs/Observable';
import {Router} from '@angular/router';

import {API_BASE_URL} from "../../../src/constants"



@Injectable()

export class PaymentsService{

    constructor( private http: Http){
        console.log("OrdersService Initialized ...");
    }

    
    handleError(error){
        
        var response = JSON.parse(error._body)
        if(response.status == 0) alert(response.error_message)
        return Observable.throw(response)

    }


    createPayment(order){
        var params = {
            order_id : order.id
        };
        var url = API_BASE_URL+"/payments";
        var headers = new Headers();
        headers.append("Authorization","Bearer mockToken");
        return this.http.post(url,params,{ headers : headers})
        .map(res => res.json()).catch(this.handleError)
    }

    checkHealth(){
        var url = API_BASE_URL+"/payments/health";
        return this.http.get(url)
        .map(res => res.json()).catch(this.handleError)
    }
}